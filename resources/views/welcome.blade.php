

<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">

	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script> -->
		<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
		
		<script>
      $(document).ready(function() {

	let object_for_send = {};
	let role_names = ['ROLE_NAME', 'ROLE_DESCRIPTION', "OPTIONS"];

	$('#add_role_fields').on('click', function() {
		let clone_var = $('#role_for_clone').clone();
		$('#role_for_clone').after(clone_var);
	});

	$('#add_options_fields').on('click', function() {
		let clone_var = $('#option_for_clone').clone();
		$('#option_for_clone').after(clone_var);
	});

	$('#generate_document').on('click', function(e) {
		$('input, textarea').each(function() {
			if(role_names.includes(this.name) == false){
				object_for_send[this.name] = this.value;
			}
		});

		let rn_length = $('[name="ROLE_NAME"]').length;
		let od_length = $('[name="OPTIONS"]').length;
		

		let all_role_names = [];
		$('[name="ROLE_NAME"]').each(function() {
			all_role_names.push($(this).val());
		});


		let all_role_descriptions = [];

		$('[name="ROLE_DESCRIPTION"]').each(function() {
			all_role_descriptions.push($(this).val());
		});


		let all_options_descriptions = [];

		$('[name="OPTIONS"]').each(function() {
			all_options_descriptions.push($(this).val());
		});




		let getValuesForRoles = function() {
			let whole_object = {};
			let arn_counter = 0;

			for(let counter = 1; counter <= rn_length; counter++)
			{
				let role_string = "ROLE_" + counter;
				let role_obj = {
					ROLE_NAME: all_role_names[arn_counter],
					ROLE_DESCRIPTION: all_role_descriptions[arn_counter],
				};
				whole_object[role_string] = role_obj;

				arn_counter++;
			}
			return whole_object;
		};

		object_for_send['ROLES'] = getValuesForRoles();




		let getValuesForOptions = function() {
			let arn_counter = 0;
			let whole_object = {};

			for(let counter = 1; counter <= od_length; counter++)
			{
				let o_string = "OPTION_" + counter;

				whole_object[o_string] = all_options_descriptions[arn_counter];

				arn_counter++;
			}
			return whole_object;
		};

		object_for_send['OPTION_DESCRIPTION'] = getValuesForOptions();

		$.post('/api/generateDocument', {object_for_send}, function(data) {
			window.location = data;
		});

	});
});

    </script>

		<title>DocGenerator</title>
		<style>
			legend {
				padding-top: 10px;
				padding-bottom: 10px;
			}
		</style>
	</head>	
	<body>
		<div class="container">
			<form class="form-horizontal" id="doc_form">
<fieldset>
<!-- Form Name -->
<legend>
	Fill Document Template 
	<span class="pull-right">
		<a href="#" class="btn btn-md btn-success" id="generate_document">Generate Document</a>
	</span>
</legend>

<!-- Text input-->
<div class="row">
<div class="form-group col-md-6">
  <label for="DOCUMENT_TITLE">Document title</label>
  
    <input id="DOCUMENT_TITLE" name="DOCUMENT_TITLE" type="text" placeholder="" class="input-xlarge form-control">
    <p class="help-block">Insert ${DOCUMENT_TITLE}</p>
  
</div>

<!-- Text input-->
<div class="form-group pull-right col-md-6">
  <label for="DOCUMENT_SHORT_DESC">Document Subtitle</label>
  
    <input id="DOCUMENT_SHORT_DESC" name="DOCUMENT_SHORT_DESC" type="text" placeholder="" class="input-xlarge form-control">
    <p class="help-block">Insert ${DOCUMENT_SHORT_DESC}</p>
  
</div>

<!-- Text input-->
<div class="form-group col-md-6">
  <label for="CURRENT_DATE">Current Date</label>
  <div class="controls">
    <input id="CURRENT_DATE" name="CURRENT_DATE" type="text" placeholder="" class="input-xlarge form-control">
    <p class="help-block">Insert ${CURRENT_DATE}</p>
  </div>
</div>

<!-- Text input-->
<div class="form-group pull-right col-md-6">
  <label for="DOCUMENT_NUMBER">Document Number</label>
  <div class="controls">
    <input id="DOCUMENT_NUMBER" name="DOCUMENT_NUMBER" type="text" placeholder="" class="input-xlarge form-control">
    <p class="help-block">Insert ${DOCUMENT_NUMBER}</p>
  </div>
</div>

<!-- Text input-->
<div class="form-group col-md-6">
  <label for="DOCUMENT_VERSION">Document Version</label>
  <div class="controls">
    <input id="DOCUMENT_VERSION" name="DOCUMENT_VERSION" type="text" placeholder="" class="input-xlarge form-control">
    <p class="help-block">Insert ${DOCUMENT_VERSION}</p>
  </div>
</div>

<!-- Text input-->
<div class="form-group pull-right col-md-6">
  <label for="DOCUMENT_VALIDITY">Document Validity</label>
  <div class="controls">
    <input id="DOCUMENT_VALIDITY" name="DOCUMENT_VALIDITY" type="text" placeholder="" class="input-xlarge form-control">
    <p class="help-block">Insert ${DOCUMENT_VALIDITY}</p>
  </div>
</div>

<!-- Text input-->
<div class="form-group col-md-6">
  <label for="CUSTOMER_NAME">Customer Name</label>
  <div class="controls">
    <input id="CUSTOMER_NAME" name="CUSTOMER_NAME" type="text" placeholder="" class="input-xlarge form-control">
    <p class="help-block">Insert ${CUSTOMER_NAME}</p>
  </div>
</div>


</div>

<div class="row">
<hr/>
<div class="form-group col-md-12">
	<a class="btn btn-md btn-info" id="add_options_fields">Add Options</a>
</div>
<div id="option_for_clone">
	<div class="form-group col-md-12">	
	  <label for="options">Option:</label>
	  <input type="text" id="OPTIONS" name="OPTIONS" class="form-control" />
	</div>
</div>
</div>


<div class="row">
<hr/>
<div class="form-group col-md-12">
	<a class="btn btn-md btn-info" id="add_role_fields">Add Role</a>
</div>

<div id="role_for_clone">
<div class="form-group col-md-6">
  <label for="ROLE_NAME">Role Name</label>
    <input id="ROLE_NAME" name="ROLE_NAME" type="text" placeholder="" class="input-xlarge form-control">
    <p class="help-block">Insert ${ROLE_NAME}</p>
  
</div>

<!-- Text input-->
<div class="form-group pull-right col-md-6">
  <label for="ROLE_DESCRIPTION">Role Description</label>
    <input id="ROLE_DESCRIPTION" name="ROLE_DESCRIPTION" type="text" placeholder="" class="input-xlarge form-control">
    <p class="help-block">Insert ${ROLE_DESCRIPTION}</p>
</div>
</div>

</div>



</fieldset>
</form>



		</div>
	</body>
</html>