<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class GeneratorController extends Controller
{
    // Method that generates the document
    public function generateDocument (Request $request)
    {
        // Load the Document template that needs to be filled
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('Team5_Template.docx');

        $variablesArray = $templateProcessor->getVariables();
        
        // Loop thorugh the variables that are defined in the template
        foreach($variablesArray as $key => $variable){
            if(!is_array($request->get($variable))){   
                $templateProcessor->setValue($variable, $request->get($variable));
            }
        }
        // Save the file with the filled tags in another document
        $templateProcessor->saveAs('Team5_Document_without_dynamic_elements.docx');
        
        $phpWordWithTags = \PhpOffice\PhpWord\IOFactory::load('Team5_Document_without_dynamic_elements.docx');
        $finalPhpWord = new \PhpOffice\PhpWord\PhpWord();
        
        // Configurations for the bullet sections
        $finalPhpWord->addNumberingStyle(
        'multilevel',
        array(
            'type' => 'multilevel',
            'levels' => array(
                        array('format' => 'bullet', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    )
            )
        );
        
        // Go through the sections in the document to locate the dynamic sections
        foreach($phpWordWithTags->getSections() as $key => $sections){
            // The section with the bullets is second in the document
            if($key == 2){
                $newSectionForOptionDescription = $finalPhpWord->addSection();
                $newSectionForOptionDescription->getStyle()->setBreakType('continuous');
                // Loop all the options that came in the request
                foreach($request->get('OPTION_DESCRIPTION') as $key => $optionDescription){
                    $newSectionForOptionDescription->addListItem($optionDescription, 0, null);
                }
            } else if($key == 4) { // The section with the roles table is fourth in the document
                $newSectionForRoles = $finalPhpWord->addSection();
                $newSectionForRoles->getStyle()->setBreakType('continuous');
                // Configurations for the table sections
                $tableStyle = array(
                            'borderColor' => '#000000',
                            'borderSize' => 2
                            );
                $firstRowStyle = array('bgColor' => '#999999');
                $finalPhpWord->addTableStyle('roles_table', $tableStyle, $firstRowStyle);
                $table = $newSectionForRoles->addTable('roles_table');
                
                // Configure first row
                $table->addRow('100',array('borderSize' => 2, 'bgColor' => '#555555'));
                $table->addCell('4000',array('bold'=>true))->addText("Role");
                $table->addCell('4000',array('bold'=>true))->addText("Description");
                
                // Loop all the roles that came in the request 
                foreach($request->get('ROLES') as $roles){
                    $table->addRow('100',array('borderSize'=>2));
                    foreach($roles as $nested_key => $nested_value){
                        $table->addCell('4000',array('bold'=>true))->addText($nested_value);
                    }
                }
            } else {
                // For all other sections that were already filled with the tags, 
                // just copy the section and paste it in the new final document
                $finalPhpWord->addCompleteSection($phpWordWithTags->getSections()[$key], $key);
            }
        }
        
        // Create the document and store it
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($finalPhpWord, 'Word2007');
        $objWriter->save('Team5_Document.docx');
        die('Download the generated <a href="Team5_Document.docx">DOCUMENT</a>');
    }
}